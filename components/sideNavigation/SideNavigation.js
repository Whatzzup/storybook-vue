export default {
  mixins: [],
  props: {
    active: {
      type: Boolean,
      default: true
    }
  },
  data() {
    return {
      sideNavItems: []
    }
  },
  created() {
    this.calcSideNav()
  },
  computed: {},
  methods: {
    calcSideNavName(name){
      var n = name.lastIndexOf("-");
      return name.substring(n + 1)
    },
    calcSideNav() {
      this.$router.options.routes.forEach((route) => {
        let subNav = []
        if (route.children) subNav = this.calcSubNav(route.children)
        this.sideNavItems.push({
          pathName: route.name,
          title: route.name,
          routePath: route.path,
          subNav
        })
      })
    },
    calcSubNav(route) {
      return route.map((routeTmp) => {
        return {
          pathName: routeTmp.name,
          title: routeTmp.name,
          routePath: routeTmp.path
        }
      })
    }
  }
}
